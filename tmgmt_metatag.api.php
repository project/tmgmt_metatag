<?php

/**
 * @file
 * Hooks provided by the TMGMT Metatag module
 */

/**
 * Implements hook_tmgmt_metatag_translating_values_alter().
 *
 * @param string $entity_type
 *   Entity type.
 * @param mixed $entity
 *   Entity object.
 * @param string[] $metatag_translating_values
 *   Array of metatag values which will translated.
 * @param array $metatag_info
 *   Entity metatag info.
 * @param string $langcode
 *   Target language of the translation job.
 */
function hook_tmgmt_metatag_translating_values_alter($entity_type, $entity, &$metatag_translating_values, $metatag_info, $langcode) {
  // Removing title from translating values list.
  if (isset($metatag_translating_values['title'])) {
    unset($metatag_translating_values['title']);
  }
}
