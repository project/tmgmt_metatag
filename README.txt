--------------------------------------------------------------------------------
  tmgmt_metatag module Readme
  http://drupal.org/project/tmgmt_metatag
--------------------------------------------------------------------------------

Contents:
=========
1. ABOUT
2. INSTALLATION
3. CREDITS

1. ABOUT
========

This module provides possibility to translate node metatags via TMGMT (metatags
will be extracted from "parent" entity and added to the same job);

2. INSTALLATION
===============

Install as usual, see http://drupal.org/node/895232 for further information.

3. CREDITS
==========

Project page: http://drupal.org/project/tmgmt_metatag

- Drupal 7 -

Authors:
* Yudkin Evgeny - https://www.drupal.org/u/evgeny_yudkin
